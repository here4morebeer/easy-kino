package easy.kino;

import easy.kino.model.Database;
import easy.kino.model.state.EasyKinoState;
import easy.kino.router.ITransitionAnimation;
import easy.kino.router.RouterApplication;
import easy.kino.router.TransitionAnimation;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.util.Duration;

public class Main extends RouterApplication {
	private static Main instance = null;

	public static Main getInstance() {
		if(instance == null) {
			throw new IllegalStateException();
		}
		return instance;
	}

	public Database db;
	public EasyKinoState state;

	final Duration TRANSITION_DURATION = new Duration(650);

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void initialize() {
		instance = this;
		this.db = new Database();
		this.state = new EasyKinoState();
		this.state.initialize();
	}

	@Override
	public void loaded() {
		redirect("IdleScene", slideLeft());
	}

	@Override
	public Stage constrcutStage(Stage primaryStage) {
		Scene scene = new Scene(root, 1600, 900);
		scene.getStylesheets().add(getClass().getResource("/styles.css").toExternalForm());
		primaryStage.setTitle("Easy Kino");
		primaryStage.setResizable(false);
		primaryStage.setScene(scene);
		return primaryStage;
	}

	@Override
	public Parent constrcutRootNode() {
		BorderPane pane = new BorderPane();
		pane.setId("root");
		return pane;
	}

	@Override
	public void injectView(Parent viewRoot) {
		viewRoot.getStyleClass().add("scene");
		((BorderPane) root).setCenter(viewRoot);
	}

	public ITransitionAnimation slideLeft() {
		return TransitionAnimation.slideLeft(TRANSITION_DURATION);
	}

	public ITransitionAnimation slideRight() {
		return TransitionAnimation.slideRight(TRANSITION_DURATION);
	}
}
