package easy.kino.controller;

import easy.kino.Main;
import easy.kino.model.Seat;
import easy.kino.model.builder.SeatBuilder;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.control.Label;
import javafx.scene.paint.Color;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class SeatSelectionController extends ControllableController implements IControllablePreviousBtn, IControllableNextBtn {
	@FXML
	GridPane seatGrid;

	ArrayList<Seat> selectedSeats;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		super.initialize(location, resources);

		selectedSeats = new ArrayList<Seat>(Main.getInstance().state.getSelectedSeats());
		render();
	}

	private void render() {
		renderSeatGrid();
	}

	private void renderSeatGrid() {
		int mrows = 5;
		int mcols = 10;

		for(int col = 0; col < mcols; col++) {
			Label lblCol = new Label(String.valueOf(col + 1));
			seatGrid.add(lblCol, 0, col);

			for(int row = 0; row < mrows; row++) {
				Seat seat = new SeatBuilder().col(col).row(row).build();
				Label lblRow = new Label(String.valueOf(row + 1));

				if(selectedSeats.contains(seat)) {
					lblRow.setBackground(new Background(new BackgroundFill(Color.GREY, CornerRadii.EMPTY, Insets.EMPTY)));
				} else {
					lblRow.setBackground(new Background(new BackgroundFill(Color.GREEN, CornerRadii.EMPTY, Insets.EMPTY)));
				}

				lblRow.setPadding(new Insets(10));
				lblRow.setOnMouseClicked(new EventHandler<MouseEvent>() {
					@Override
					public void handle(MouseEvent event) {
						lblRow.setBackground(new Background(new BackgroundFill(Color.GREY, CornerRadii.EMPTY, Insets.EMPTY)));
						selectedSeats.add(seat);
						currentStateUpdated();
					}
				});
				seatGrid.add(lblRow, row + 1, col);
			}
		}
	}

	@Override
	public boolean currentStateValid() {
		if(selectedSeats.size() > 0) {
			return true;
		}
		return false;
	}

	@Override
	public void next() {
		Main.getInstance().state.setSelectedSeats(selectedSeats);
		nextView("BookingScene");
	}

	@Override
	public void previous() {
		Main.getInstance().state.setSelectedSeats(selectedSeats);
		prevView("MovieDetailsScene");
	}
}
