package easy.kino.controller;

public interface IControllable {
	/**
	 * Checks whether the current state of the view is valid or not.
	 * @return			State is valid or not
	 */
	boolean currentStateValid();
}
