package easy.kino.controller;

import easy.kino.Constants;
import easy.kino.Main;
import easy.kino.model.AgeRestriction;
import easy.kino.model.AgeRestrictionExtension;
import easy.kino.model.Movie;
import easy.kino.model.MovieSchedule;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Font;

import java.net.URL;
import java.util.ResourceBundle;

public class OverviewController extends ControllableController implements IControllableNextBtn, IControllablePreviousBtn {
	@FXML
	TilePane overviewPane;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		super.initialize(location, resources);

		overviewPane.setHgap(20); overviewPane.setVgap(20);

		/*for (Movie movie : Main.app.db.movieCollection) {
			overviewPane.getChildren().add(constructMovieNode(movie));
		}*/

		for (MovieSchedule schedule : Main.getInstance().db.getMovieSchedulesWatchableToday()) {
			overviewPane.getChildren().add(contructMovieScheduleNode(schedule));
		}

	}

	private Node contructMovieScheduleNode(MovieSchedule schedule) {
		HBox container = new HBox();
		container.getStyleClass().add("movie");
		container.setSpacing(10);

		Movie movie = Main.getInstance().db.getMovieById(schedule.movieId);
		if (movie == null) {
			throw new IllegalArgumentException();
		}

		container.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Main.getInstance().state.setSelectedMovie(movie);
				Main.getInstance().state.setSelectedSchedule(schedule);

				clearSelectedMovie();
				container.getStyleClass().add("selected");
			}
		});

		// Select schedule if is equal to state schedule
		if (Main.getInstance().state.getSelectedSchedule() != null && Main.getInstance().state.getSelectedSchedule().equals(schedule)) {
			container.getStyleClass().add("selected");
		}

		ImageView movieConverImageView = new ImageView(movie.getCoverImage());
		movieConverImageView.setFitWidth(150);
		movieConverImageView.setFitHeight(225);

		// Description Container
		VBox movieDescriptionContainer = new VBox();
		movieDescriptionContainer.setSpacing(10);

		// Title Container
		HBox movieTitleContainer = new HBox();
		movieTitleContainer.setSpacing(20);

		// Movie Title
		Label movieTitleLabel = new Label(movie.title);
		movieTitleLabel.setFont(Constants.MOVIE_OVERVIEW_TITLE_FONT);

		movieTitleContainer.getChildren().add(movieTitleLabel);

		// Age Rating
		if (movie.ageRestriction != null && movie.ageRestriction != AgeRestriction.ALL_AGES) {
			ImageView movieAgeRatingImage = new ImageView(AgeRestrictionExtension.getIcon(movie.ageRestriction));
			movieAgeRatingImage.setFitHeight(40);
			movieAgeRatingImage.setFitWidth(40);
			movieTitleContainer.getChildren().add(movieAgeRatingImage);
		}

		// Movie Rating
		HBox movieRatingContainer = new HBox();
		for (int i = 0; i < movie.rating; i++) {
			ImageView movieRatingStarImage = new ImageView(new Image(getClass().getResource("/img/rating/star.png").toString()));
			movieRatingStarImage.setFitHeight(25);
			movieRatingStarImage.setFitWidth(25);
			movieRatingContainer.getChildren().add(movieRatingStarImage);
		}

		// Screening Time
		Label movieScreeningTimeLabel = new Label(Constants.MOVIE_SCHEDULE_TIME_FORMAT.format(schedule.from) + " - " + Constants.MOVIE_SCHEDULE_TIME_FORMAT.format(schedule.till));
		movieScreeningTimeLabel.setFont(Constants.MOVIE_OVERVIEW_SCREENING_FONT);

		movieDescriptionContainer.getChildren().addAll(movieTitleContainer, movieRatingContainer, movieScreeningTimeLabel);
		container.getChildren().addAll(movieConverImageView, movieDescriptionContainer);

		return container;
	}

	private Node constructMovieNode(Movie movie) {
		HBox root = new HBox();
		root.getStyleClass().add("movie");
		if (Main.getInstance().state.getSelectedMovie() != null && Main.getInstance().state.getSelectedMovie().id == movie.id) {
			root.getStyleClass().add("selected");
		}
		root.setSpacing(10);
		root.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Main.getInstance().state.setSelectedMovie(movie);
				clearSelectedMovie();
				root.getStyleClass().add("selected");
			}
		});

		// Cover Image
		ImageView thumbImage = new ImageView(movie.getCoverImage());
		thumbImage.setFitHeight(225);
		thumbImage.setFitWidth(150);

		VBox descRoot = new VBox();
		descRoot.setSpacing(10);

		HBox movieTitleWrapper = new HBox();
		movieTitleWrapper.setSpacing(20);

		// Movie Title
		Label movieTitle = new Label(movie.title);
		movieTitle.setFont(new Font("Arial", 25));

		movieTitleWrapper.getChildren().add(movieTitle);

		// Age Rating
		if (movie.ageRestriction != null && movie.ageRestriction != AgeRestriction.ALL_AGES) {
			ImageView movieAgeRating = new ImageView(AgeRestrictionExtension.getIcon(movie.ageRestriction));
			movieAgeRating.setFitHeight(40);
			movieAgeRating.setFitWidth(40);
			movieTitleWrapper.getChildren().add(movieAgeRating);
		}

		// Movie Rating
		HBox ratingPane = new HBox();
		for (int i = 0; i < movie.rating; i++) {
			ImageView ratingStarImage = new ImageView(new Image(getClass().getResource("/img/rating/star.ico").toString()));
			ratingStarImage.setFitHeight(25);
			ratingStarImage.setFitWidth(25);

			ratingPane.getChildren().add(ratingStarImage);
		}

		descRoot.getChildren().addAll(movieTitleWrapper, ratingPane);
		root.getChildren().addAll(thumbImage, descRoot);
		return root;
	}

	private void clearSelectedMovie() {
		for(Node node : overviewPane.getChildren()) {
			if(node.getStyleClass().contains("selected")) {
				node.getStyleClass().clear();
				node.getStyleClass().add("movie");
			}
		}
	}

	@Override
	public void previous() {
		Main.getInstance().state.setSelectedMovie(null);
		prevView("IdleScene");
	}

	@Override
	public void next() {
		nextView("MovieDetailsScene");
	}

	@Override
	public boolean currentStateValid() {
		return Main.getInstance().state.getSelectedMovie() != null;
	}
}
