package easy.kino.controller;

public interface IControllablePreviousBtn {
	/**
	 * Redirect to previous view.
	 */
	void previous();
}
