package easy.kino.controller;

public interface IControllableNextBtn {
	/**
	 * Redirect to next view.
	 */
	void next();
}
