package easy.kino.controller;

import javafx.scene.Parent;

public interface IWrapper {
	Parent wrap(Parent node);
}
