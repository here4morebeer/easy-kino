package easy.kino.controller;

import easy.kino.Constants;
import easy.kino.Main;
import easy.kino.model.Movie;
import easy.kino.model.MovieSchedule;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

import java.net.URL;
import java.util.ResourceBundle;

public class MovieDetailsController extends ControllableController implements IControllablePreviousBtn, IControllableNextBtn {
	@FXML
	ImageView movieCoverImage;

	@FXML
	Label movieTitleLabel;

	@FXML
	TextArea movieDescriptionArea;

	@FXML
	VBox paneTimes;

	private Movie selectedMovie;
	private MovieSchedule selectedSchedule;

	private void render() {
		if (Main.getInstance().state.getSelectedMovie() == null) {
			throw new IllegalStateException("SelectedMovie cannot be null");
		}

		movieCoverImage.setImage(selectedMovie.getCoverImage());
		movieTitleLabel.setText(selectedMovie.title);
		movieDescriptionArea.setText(selectedMovie.description);

		renderTimes();
	}

	private void renderTimes() {
		paneTimes.getChildren().clear();
		for(MovieSchedule schedule : Main.getInstance().db.getMovieSchedulesByMovieId(selectedMovie.id)) {
			CheckBox checkBox = new CheckBox(Constants.MOVIE_SCHEDULE_TIME_FORMAT.format(schedule.from) + " - " + Constants.MOVIE_SCHEDULE_TIME_FORMAT.format(schedule.till));
			if(schedule == selectedSchedule) {
				checkBox.setSelected(true);
			}
			checkBox.setFont(Constants.MOVIE_OVERVIEW_SCREENING_FONT);
			checkBox.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					Main.getInstance().state.setSelectedSchedule(schedule);
				}
			});
			paneTimes.getChildren().add(checkBox);
		}
	}

	@Override
	public void currentStateUpdated() {
		super.currentStateUpdated();

		this.selectedMovie = Main.getInstance().state.getSelectedMovie();
		this.selectedSchedule = Main.getInstance().state.getSelectedSchedule();

		render();
	}

	@Override
	public boolean currentStateValid() {
		if(Main.getInstance().state.getSelectedMovie() != null && Main.getInstance().state.getSelectedSchedule() != null) {
			return true;
		}
		return false;
	}

	@Override
	public void previous() {
		prevView("OverviewScene");
	}

	@Override
	public void next() {
		nextView("SeatSelectionScene");
	}
}
