package easy.kino.controller;

import easy.kino.Main;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;

import java.net.URL;
import java.util.ResourceBundle;

public class IdleController extends Controller {
	@FXML
	BorderPane idleRoot;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		super.initialize(location, resources);

		idleRoot.addEventHandler(MouseEvent.MOUSE_CLICKED, new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent event) {
				Main.getInstance().redirect("OverviewScene", Main.getInstance().slideLeft());
			}
		});
	}
}
