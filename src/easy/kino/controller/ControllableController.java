package easy.kino.controller;

import easy.kino.Main;
import javafx.event.ActionEvent;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.layout.BorderPane;

public abstract class ControllableController extends Controller implements IControllable, IWrapper {
	Button btnPrevious;
	Button btnNext;

	@Override
	public void currentStateUpdated() {
		super.currentStateUpdated();

		if(btnNext != null) {
			btnNext.setDisable(!currentStateValid());
		}
	}

	@Override
	public Parent wrap(Parent node) {
		BorderPane root = new BorderPane();

		BorderPane controlBar = new BorderPane();
		controlBar.setId("controlBar");
		controlBar.setPrefSize(Double.MAX_VALUE, 90);

		if (this instanceof IControllableNextBtn) {
			btnNext = new Button("Next");
			btnNext.setOnAction(this::btnNextAction);
			btnNext.getStyleClass().add("btn");
			btnNext.setDisable(!currentStateValid());
			controlBar.setRight(btnNext);
		}

		if (this instanceof IControllablePreviousBtn) {
			btnPrevious = new Button("Previous");
			btnPrevious.setOnAction(this::btnPreviousAction);
			btnPrevious.getStyleClass().add("btn");
			controlBar.setLeft(btnPrevious);
		}

		root.setCenter(node);
		root.setBottom(controlBar);
		return root;
	}

	public void nextView(String view) {
		Main.getInstance().redirect(view, Main.getInstance().slideLeft());
	}

	public void prevView(String view) {
		Main.getInstance().redirect(view, Main.getInstance().slideRight());
	}

	protected void btnNextAction(ActionEvent actionEvent) {
		((IControllableNextBtn) this).next();
	}
	protected void btnPreviousAction(ActionEvent actionEvent) {
		((IControllablePreviousBtn) this).previous();
	}
}
