package easy.kino.controller;

public class BookingController extends ControllableController implements IControllablePreviousBtn {

	@Override
	public boolean currentStateValid() {
		return false;
	}

	@Override
	public void previous() {
		prevView("SeatSelectionScene");
	}
}
