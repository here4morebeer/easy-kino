package easy.kino.controller;

import easy.kino.Main;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.Observable;
import java.util.Observer;
import java.util.ResourceBundle;

/**
 * Provides all controller with misc functions.
 */
public abstract class Controller implements Initializable {

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		Main.getInstance().state.addObserver(new Observer() {
			@Override
			public void update(Observable o, Object arg) {
				currentStateUpdated();
			}
		});
		currentStateUpdated();
	}

	/**
	 * State of the Application has been updated.
	 */
	public void currentStateUpdated() {

	}
}

