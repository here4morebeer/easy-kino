package easy.kino.router;

import easy.kino.controller.Controller;
import easy.kino.controller.IWrapper;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public abstract class RouterApplication extends Application {
	protected Parent root;

	private boolean loadingView = false;
	private Parent loadedView;
	private Parent currentView;

	public void redirect(String view) {
		redirect(view, TransitionAnimation.none());
	}

	public void redirect(String view, ITransitionAnimation animation) {
		if(loadingView) return;
		loadingView = true;

		loadView(view);
		animation.animate(this, loadedView, () -> {
			currentView = loadedView;
			injectView(loadedView);
			loadingView = false;
		});
	}

	private void loadView(String view) {
		FXMLLoader loader = new FXMLLoader(getClass().getResource("/view/" + view + ".fxml"));

		Parent tmpView;
		try {
			tmpView = loader.load();
		} catch (IOException ex) {
			throw new Error(ex);
		}

		Controller loadedController = loader.getController();
		if (loadedController instanceof IWrapper) {
			tmpView = ((IWrapper) loader.getController()).wrap(tmpView);
		}

		loadedView = tmpView;
	}

	@Override
	public void start(Stage primaryStage) {
		initialize();
		root = constrcutRootNode();
		primaryStage = constrcutStage(primaryStage);
		primaryStage.show();
		loaded();
	}

	public Stage constrcutStage(Stage primaryStage) {
		primaryStage.setScene(new Scene(root));
		return primaryStage;
	}

	public Parent getCurrentView() {
		return this.currentView;
	}

	public Parent getLoadedView() {
		return this.loadedView;
	}

	public abstract Parent constrcutRootNode();

	public abstract void injectView(Parent viewRoot);

	public void initialize() {

	}

	public void loaded() {

	}
}
