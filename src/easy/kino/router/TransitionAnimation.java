package easy.kino.router;

import javafx.animation.TranslateTransition;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.scene.Parent;
import javafx.util.Duration;

public abstract class TransitionAnimation {
	public static ITransitionAnimation none() {
		return new ITransitionAnimation() {
			@Override
			public void animate(RouterApplication app, Parent newView, Runnable done) {
				done.run();
			}
		};
	}

	public static ITransitionAnimation slideLeft(Duration duration) {
		return new ITransitionAnimation() {
			final double FADE_OUT_X = -2000;
			final double FADE_IN_X = 2000;

			@Override
			public void animate(RouterApplication app, Parent newView, Runnable done) {
				EventHandler animHandler = new EventHandler() {
					@Override
					public void handle(Event event) {
						app.getLoadedView().setTranslateX(FADE_IN_X);
						done.run();

						TranslateTransition outAn = new TranslateTransition(duration, app.getCurrentView());
						outAn.setFromX(FADE_IN_X);
						outAn.setToX(0);
						outAn.play();
					}
				};

				if (app.getCurrentView() != null) {
					TranslateTransition outAn = new TranslateTransition(duration, app.getCurrentView());
					outAn.setFromX(0);
					outAn.setToX(FADE_OUT_X);
					outAn.setOnFinished(animHandler);
					outAn.play();
				} else {
					animHandler.handle(null);
				}
			}
		};
	}

	public static ITransitionAnimation slideRight(Duration duration) {
		return new ITransitionAnimation() {
			final double FADE_OUT_X = 2000;
			final double FADE_IN_X = -2000;

			@Override
			public void animate(RouterApplication app, Parent newView, Runnable done) {
				EventHandler animHandler = new EventHandler() {
					@Override
					public void handle(Event event) {
						app.getLoadedView().setTranslateX(FADE_IN_X);
						done.run();

						TranslateTransition outAn = new TranslateTransition(duration, app.getCurrentView());
						outAn.setFromX(FADE_IN_X);
						outAn.setToX(0);
						outAn.play();
					}
				};

				if (app.getCurrentView() != null) {
					TranslateTransition outAn = new TranslateTransition(duration, app.getCurrentView());
					outAn.setFromX(0);
					outAn.setToX(FADE_OUT_X);
					outAn.setOnFinished(animHandler);
					outAn.play();
				} else {
					animHandler.handle(null);
				}
			}
		};
	}
}
