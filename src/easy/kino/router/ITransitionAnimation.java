package easy.kino.router;

import javafx.scene.Parent;

public interface ITransitionAnimation {
	void animate(RouterApplication app, Parent newView, Runnable done);
}
