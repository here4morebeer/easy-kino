package easy.kino;

import javafx.scene.text.Font;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public abstract class Constants {
	public static DateFormat MOVIE_SCHEDULE_TIME_FORMAT = new SimpleDateFormat("HH:mm");
	public static Font MOVIE_OVERVIEW_SCREENING_FONT = new Font("Arial", 25);
	public static Font MOVIE_OVERVIEW_TITLE_FONT = new Font("Arial", 30);
}
