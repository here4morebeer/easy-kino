package easy.kino.model.state;

import easy.kino.model.Movie;
import easy.kino.model.MovieSchedule;
import easy.kino.model.Seat;

import java.util.ArrayList;
import java.util.Collection;

public class EasyKinoState extends ApplicationState {
	private Movie selectedMovie;
	private MovieSchedule selectedSchedule;
	private Collection<Seat> selectedSeats;

	@Override
	public void initialize() {
		this.selectedMovie = null;
		this.selectedSchedule = null;
		this.selectedSeats = new ArrayList<Seat>();
	}

	public Movie getSelectedMovie() {
		return selectedMovie;
	}

	public void setSelectedMovie(Movie selectedMovie) {
		this.selectedMovie = selectedMovie;
		this.stateChanged();
	}

	public MovieSchedule getSelectedSchedule() {
		return selectedSchedule;
	}

	public void setSelectedSchedule(MovieSchedule selectedSchedule) {
		this.selectedSchedule = selectedSchedule;
		this.stateChanged();
	}

	public Collection<Seat> getSelectedSeats() {
		return selectedSeats;
	}

	public void setSelectedSeats(Collection<Seat> selectedSeats) {
		this.selectedSeats = selectedSeats;
		this.stateChanged();
	}
}
