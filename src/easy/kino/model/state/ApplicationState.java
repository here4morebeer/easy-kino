package easy.kino.model.state;

import java.util.Observable;

public abstract class ApplicationState extends Observable {
	public abstract void initialize();

	public void stateChanged() {
		setChanged();
		notifyObservers();
	}
}
