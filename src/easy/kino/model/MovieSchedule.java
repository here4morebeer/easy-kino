package easy.kino.model;

import java.util.Date;

public class MovieSchedule {
    public int id;
    public Date from;
    public Date till;
    public int movieId;
    public int roomId;
}
