package easy.kino.model;

import easy.kino.model.builder.MovieBuilder;
import easy.kino.model.builder.MovieScheduleBuilder;
import javafx.scene.image.Image;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

/**
 * Stores and manages all Data.
 */
public class Database {
    public Collection<Movie> movieCollection;
    public Collection<MovieSchedule> movieScheduleCollection;
    public Collection<Room> roomCollection;

    public Database() {
    	Seed();
    }

	/**
	 * Seeds all collections with placeholder data.
	 */
	public void Seed() {
        this.movieCollection = new ArrayList<Movie>();
		this.movieScheduleCollection = new ArrayList<MovieSchedule>();
		this.roomCollection = new ArrayList<Room>();

    	this.movieCollection.add(new MovieBuilder()
                .id(1).title("Emoji Movie")
                .description("Funnny Movie.").rating(3)
                .ageRestriction(AgeRestriction.ALL_AGES)
				.cover(new Image(getClass().getResource("/img/covers/emoji_movie.jpg").toString())).build());
		addScheduleForMovie(getMovieById(1));

		this.movieCollection.add(new MovieBuilder()
				.id(2).title("The Silence of the Lambs")
				.description("Funnny Movie.").rating(5)
				.ageRestriction(AgeRestriction.OVER_18)
				.cover(new Image(getClass().getResource("/img/covers/lambs.jpg").toString())).build());
		addScheduleForMovie(getMovieById(2));

		this.movieCollection.add(new MovieBuilder()
				.id(3).title("Infinity War")
				.description("Funny Movie").rating(4)
				.ageRestriction(AgeRestriction.OVER_12)
				.cover(new Image(getClass().getResource("/img/covers/infinity_war.jpeg").toString())).build());
		addScheduleForMovie(getMovieById(3));

		this.movieCollection.add(new MovieBuilder()
				.id(4).title("2012")
				.description("Not so Funny Movie").rating(4)
				.ageRestriction(AgeRestriction.OVER_12)
				.cover(new Image(getClass().getResource("/img/covers/2012.jpeg").toString())).build());
		addScheduleForMovie(getMovieById(4));

		this.movieCollection.add(new MovieBuilder()
				.id(5).title("Bad Teacher")
				.description("Funny Movie").rating(4)
				.ageRestriction(AgeRestriction.OVER_16)
				.cover(new Image(getClass().getResource("/img/covers/bad_teacher.jpeg").toString())).build());
		addScheduleForMovie(getMovieById(5));

		this.movieCollection.add(new MovieBuilder()
				.id(6).title("Angry Birds der Film")
				.description("Funny Movie").rating(4)
				.ageRestriction(AgeRestriction.OVER_6)
				.cover(new Image(getClass().getResource("/img/covers/angry_birds.jpeg").toString())).build());
		addScheduleForMovie(getMovieById(6));
		
		this.movieCollection.add(new MovieBuilder()
				.id(7).title("Ready Player One")
				.description("Funny Movie").rating(4)
				.ageRestriction(AgeRestriction.OVER_12)
				.cover(new Image(getClass().getResource("/img/covers/readyplayer_one.jpeg").toString())).build());
		addScheduleForMovie(getMovieById(7));
	}

	private void addScheduleForMovie(Movie movie) {
		Date[] dates = new Date[]{
				new Date(),
				new Date((new Date()).getTime() + (2 * (3600 * 1000)))
		};

		for (Date date : dates) {
			this.movieScheduleCollection.add(new MovieScheduleBuilder()
					.from(date)
					.till(new Date(date.getTime() + (2 * (3600 * 1000))))
					.movieId(movie.id)
					.roomId(1)
					.build());
		}
	}

	/**
	 * Returns the Movie with the corresponding id.
	 * @param	id		Id of the searched movie.
	 * @return			Movie matching the given id.
	 */
	public Movie getMovieById(int id) {
        for (Movie movie : movieCollection) {
            if(movie.id == id) {
                return movie;
            }
        }
        return null;
    }

	/**
	 * Seaches for a Movie with a given name. If nothing found it returns NULL.
	 * @param	name	Name of the movie.
	 * @return			If found movie matching the name or NULL.
	 */
	public Movie getMovieByName(String name) {
        for (Movie movie : movieCollection) {
            if(movie.title.equalsIgnoreCase(name)) {
                return movie;
            }
        }
        return null;
    }

	public MovieSchedule getMovieScheduleById(int id) {
        for (MovieSchedule movieSchedule : movieScheduleCollection) {
            if(movieSchedule.id == id) {
                return movieSchedule;
            }
        }
        return null;
    }

    public Collection<MovieSchedule> getMovieSchedulesByMovieId(int id) {
        Collection<MovieSchedule> schedules = new ArrayList<MovieSchedule>();
        for (MovieSchedule movieSchedule : movieScheduleCollection) {
            if(movieSchedule.movieId == id) {
                schedules.add(movieSchedule);
            }
        }
        return schedules;
    }

    public Room getRoomByMovieScheduleId(int id) {
        MovieSchedule schedule = getMovieScheduleById(id);
        return getRoomById(schedule.roomId);
    }

    public Room getRoomById(int id) {
        for (Room room : roomCollection) {
            if(room.id == id) {
                return room;
            }
        }
        return null;
    }

	public Collection<MovieSchedule> getMovieSchedulesWatchableToday() {
		// TODO: Implement logic
		return this.movieScheduleCollection;
	}
}
