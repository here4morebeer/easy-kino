package easy.kino.model;

public enum RoomSizeType {
    TINY,
    BIG
}
