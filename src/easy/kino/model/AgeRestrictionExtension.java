package easy.kino.model;

import javafx.scene.image.Image;

public abstract class AgeRestrictionExtension {
	public static Image getIcon(AgeRestriction res) {
		return new Image(getIconResPath(res));
	}

	public static String getIconResPath(AgeRestriction res) {
		String pf = "/img/fsk";
		switch (res) {
			case ALL_AGES:
				return AgeRestrictionExtension.class.getResource(pf + "/FSK_0.png").toString();
			case OVER_6:
				return AgeRestrictionExtension.class.getResource(pf + "/FSK_6.png").toString();
			case OVER_12:
				return AgeRestrictionExtension.class.getResource(pf + "/FSK_12.png").toString();
			case OVER_16:
				return AgeRestrictionExtension.class.getResource(pf + "/FSK_16.png").toString();
			case OVER_18:
				return AgeRestrictionExtension.class.getResource(pf + "/FSK_18.png").toString();
		}
		return null;
	}
}
