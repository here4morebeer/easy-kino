package easy.kino.model;

public enum AgeRestriction {
    ALL_AGES,
    OVER_6,
    OVER_12,
    OVER_16,
    OVER_18
}
