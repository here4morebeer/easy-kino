package easy.kino.model;

import javafx.scene.image.Image;

public class Movie {
    public int id;
    public String title;
    public String description;
    public int rating;
    public AgeRestriction ageRestriction;
    public Image cover;

	public Image getCoverImage() {
		return this.cover != null ? this.cover : new Image(getClass().getResourceAsStream("/img/covers/notfound.jpg"));
	}
}
