package easy.kino.model.builder;

import easy.kino.model.AgeRestriction;
import easy.kino.model.Movie;
import javafx.scene.image.Image;

public class MovieBuilder extends Builder<Movie> {
	public MovieBuilder() {
		this.model = new Movie();
	}
	public MovieBuilder id(int v) { this.model.id = v; return this; }
	public MovieBuilder title(String v) { this.model.title = v; return this; }
	public MovieBuilder description(String v) { this.model.description = v; return this; }
	public MovieBuilder rating(int v) { this.model.rating = v; return this; }
	public MovieBuilder ageRestriction(AgeRestriction v) { this.model.ageRestriction = v; return this; }
	public MovieBuilder cover(Image v) { this.model.cover = v; return this; }
}
