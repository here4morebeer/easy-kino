package easy.kino.model.builder;

import easy.kino.model.Room;
import easy.kino.model.RoomSizeType;
import easy.kino.model.Seat;

public class SeatBuilder extends Builder<Seat> {
	public SeatBuilder() {
		this.model = new Seat();
	}
	public SeatBuilder row(int v) { this.model.row = v; return this; }
	public SeatBuilder col(int v) { this.model.col = v; return this; }
	public SeatBuilder room(Room v) { this.model.room = v; return this; }
}
