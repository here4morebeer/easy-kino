package easy.kino.model.builder;

import easy.kino.model.MovieSchedule;
import easy.kino.model.Room;
import easy.kino.model.RoomSizeType;

import java.util.Date;

public class RoomBuilder extends Builder<Room> {
	public RoomBuilder() {
		this.model = new Room();
	}
	public RoomBuilder id(int v) { this.model.id = v; return this; }
	public RoomBuilder size(RoomSizeType v) { this.model.sizeType = v; return this; }
}
