package easy.kino.model.builder;

public abstract class Builder<T> {
	protected T model;
	public T build() {
		return this.model;
	}
}
