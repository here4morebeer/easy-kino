package easy.kino.model.builder;

import easy.kino.model.MovieSchedule;

import java.util.Date;

public class MovieScheduleBuilder extends Builder<MovieSchedule> {
	public MovieScheduleBuilder() {
		this.model = new MovieSchedule();
	}
	public MovieScheduleBuilder id(int v) { this.model.id = v; return this; }
	public MovieScheduleBuilder from(Date v) { this.model.from = v; return this; }
	public MovieScheduleBuilder till(Date v) { this.model.till = v; return this; }
	public MovieScheduleBuilder movieId(int v) { this.model.movieId = v; return this; }
	public MovieScheduleBuilder roomId(int v) { this.model.roomId = v; return this; }
}
